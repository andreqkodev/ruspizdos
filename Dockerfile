FROM php:7.4-fpm

COPY ["main.php", "main.php"]

ENTRYPOINT ["php", "main.php"]